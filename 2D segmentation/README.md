`` 2D Segmentation``

##Description
This folder contains scripts for 2D image segmentation. In order to run the `example_segmentation`, MATLAB 6.5 or a later version has to be installed and you have also to download the random_walker package from
http://cns.bu.edu/~lgrady/software.html. Installation of the Graph Analysis toolbox is also required, which shall be downloaded from http://eslab.bu.edu/software/graphanalysis/.

The function performs image segmentaion based on seeds, with the help of the random walker algorithm. Seeds refer to a small number of pixels with known labels (e.g. object and background). 
The procedure works as a graph based method; i.e. the pixels of the image constitute the nodes of the graph that are connected with weighted edges. Given a small number of pixels with pre-defined labels, 
one can analytically and quickly determine the probability that a random walker starting at each unlabeled pixel will reach one of the pre-labeled pixels. By assigning each pixel to the label for which 
the greatest probability is calculated, pixels are assigned to specific labels and high-quality image segmentation is obtained.

More on the theory of the random walker algorithm are explained in the paper:
Leo Grady and Gareth Funka-Lea, "Multi-Label Image Segmentation for Medical Applications Based on Graph-Theoretic Electrical Potentials", in Proceedings of the 8th ECCV04,
Workshop on Computer Vision Approaches to Medical Image Analysis and Mathematical Methods in Biomedical Image Analysis, p. 230-245, May 15th, 2004, Prague, Czech Republic,
Springer-Verlag. Available at: http://cns.bu.edu/~lgrady/grady2004multilabel.pdf

The provided `example_segmentation.m` demonstrates how to use the downaloaded algorithm for the purposes of the project.

