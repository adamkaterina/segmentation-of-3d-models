`` 3D Segmentation``

## Installation
In order to execute the function, PCL 1.3 or a newer version has to be installed. PCL shall be downloaded from http://pointclouds.org. PCL 
is compatible with multiple operating systems and prebuilt binaries are available for Microsoft Windows, Linux and Apple MAC OS X. In order for PCL to function, the installation of 3rd party 
libraries is needed, including boost, Eigen, Flann, VTK, Qhall, OpenNI and Qt. All in one installers, which install both PCL and its dependencies, are available for Windows and the installation
procedure is described in PCL's site.
 
A folder containing the `source` files provides the `main.cpp`, along with `converter.h` and `converter.cpp`. The data folder provides the sample `table_scene_lms400.obj` file and README.md 
is also provided for further information. The `table_scene_lms400.obj` was freely downloaded from http://pointclouds.org in the .pcd format and converted to .obj. 

## Usage
The function performs 3D segmentation based on geometric features. 

The main function's input is an .obj file and the output are different .obj files that stand for the different segmented objects, as well as a .txt file, containing the X, Y, Z coordinates
of the barycenter of each Region of Interest (i.e. each segmented objects), under the name `barycenters.txt`. A sample `barycenters.txt` is  available in the data folder.

Point clouds are segmented, employing a RANSAC based plane detection.  RANSAC is a model fitting algorithm robust to outliers which solves the problem based on the least number of observations needed.
In the case of plane detection, subsets of three points are formed and each subset of observations forms a plane.  For each of the rest observations, the distance from the plane is computed and if it falls 
within a predefined threshold, the point is consistent with the solution and considered as an inlier. The rest of the observations are considered as outliers. The final plane is computed based on a data
fitting algorithm with all the inliers, which are afterwards removed from the dataset. The procedure continues until no planes are detected. While, all the remaining observations are clustered based on 
Euclidean clustering. 

## Parameters to tune 
There are five parameters that have to be tuned correctly in order to accomplish the expected results. The first one concerns the maximum number of iterations performed, in order to identify a plane. 
More specifically, different subsets of three points and the formed planes are tested in each iteration. If the maximum number of iterations is reached and a plane has not been identified, the procedure 
continues with the Euclidean clustering. Furthermore, addressing again plane based detection, another parameter that has to be defined is the distance threshold (measured in meters), determining whether 
the observation is consistent with the solution or not. 
 
Concerning Euclidean clustering, three parameters have to be set. The first argument stands for the cluster tolerance (in meters) and the two remaining concern the minimum and maximum number of 
points that could compose a cluster. These variables have to be determined wisely according to the problem and the desired results of the segmentation, in order to avoid under or over segmentation.