## DigiArt (http://digiart-project.eu/)
`` Point cloud segmentation integrating 3D segmentation on the point cloud and 2D segmentation on the UAV images ``

This repository provides the code for the hybrid segmentation of point clouds sourcing from UAV images, combining both 3D segmentation and 2D segmentation.
3D segmentation is based on geometrical features and comprises a RANSAC plane based detection along with Euclidean clustering. Subsequently, the most appropriate
views for image segmentation are selected and image segmentation is performed, based on seeds (with the help of the random walker algorithm). Different scripts
in C++ and MATLAB has been developed and tested, using a machine with 8GB RAM and an Intel Core i5-4670K CPU processor.  

The code is organised in three sections:
- 3D segmentation
- Best Picture Selection
- 2D segmentation
    
 
## 3D segmentation
For the 3D segmentation, software has been developed using the IDE Microsoft Visual Studio 2010 and the programming language C++,
under the operating system Windows 7 Professional. The minimum version required in order to handle the distributed software is PCL 1.3 (http://pointclouds.org)

A folder containing the source files provides the `main.cpp`, along with `converter.h` and `converter.cpp`. The data folder provides a sample .obj file to 
execute a function, and README.md is also provided for further information.  

The main function's input is an .obj file and the output are different .obj files that stand for the different segmented objects, as well as a .txt file, 
containing the X, Y, Z coordinates of the barycenter of each Region of Interest (i.e. each segmented objects), under the name `barycenters.txt`.

Apart from the input file, there are also five parameters that have to be tuned correctly in order to accomplish the expected results. The first one concerns the maximum number 
of iterations performed, in order to identify a plane. More specifically, different subsets of three points and the planes formed are tested in each iteration. If the maximum number of iterations is reached and a plane has not been identified, the procedure continues with the Euclidean clustering. Furthermore, addressing again plane based detection, another parameter that has to be defined is the distance threshold (measured in meters), determining whether the observation is consistent with the solution or not. 
 
Concerning Euclidean clustering, three parameters have to be set. The first argument stands for the cluster tolerance (in meters) and the two remaining concern the minimum and maximum number of points that could compose a cluster. These variables have to be determined wisely according to the problem and the desired results of the segmentation, in order to avoid under or over segmentation. 

## Best Picture Selection
Concerning the selection of the most appropriate view, the `selectBestPicture.m` script is provided, along with `example_bestPicture.m` that demonstrates the usage
of the function. The scripts are writted in MATLAB and available under the folder `source`. 

The function `selectBestPicture.m` returns the three most appropriate views for each ROI for image segmentation. The outputs of the function are three strings.

The input arguments are the name of a .txt file containing the exterior orientation of each view, the X, Y, Z coordinates of the barycenter of a 3D ROI in meters, the focal length (c) and the coordinates of the principal point (xo,yo) in mm. 
Along with the name of a .txt file containing the names of the images and the dimensions of the images in mm (along X and Y direction).

It is important to notice that the sequence of the images in the two .txt files (the one recording the exterior orientation of each view and the one recording the names of the images) has to be the same.

Examples of the two .txt files are provided in the folder `data`. `external_parameters_test1.txt` has the following format:
- 'Xo' (m) : 1st column 
- 'Yo' (m) : 2nd column
- 'Zo' (m) : 3rd column
- 'roll' (degrees) : 4th column
- 'pitch' (degrees) : 5th column
- 'yaw' (degrees) : 6th column

While, `images.txt` contains a column with all the names of the available images.

## 2D Image Segmentation
In order to run the example_segmentation, you have to download the random_walker package from http://cns.bu.edu/~lgrady/software.html. 
Installation of the Graph Analysis toolbox is also required, which shall be downloaded from http://eslab.bu.edu/software/graphanalysis/.
The provided `example_segmentation.m` demonstrates how to use the downaloaded algorithm for the purposes of the project.

## Acknowledgement
Random Walker algorithm has been downloaded from Leo Grady's personal website http://cns.bu.edu/~lgrady/software.html and `converter.cpp` and `converter.h`
files have been downloaded from  https://github.com/PaulBernier/obj2pcd and modified for the purposes of project.