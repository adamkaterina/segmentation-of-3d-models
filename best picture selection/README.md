`` Best Picture Selection ``

##Description
The function `selectBestPicture.m` returns the three most appropriate views for each Region of Interest (ROI) for image segmentation. The outputs of the function are the image names of the most suitable views (three strings).
The scripts are written in MATLAB and available under the folder `source`.  MATLAB has to be installed for the execution of the functions.

The input arguments are the name of a .txt file containing  the exterior orientation of each view,  the X, Y, Z coordidantes of the barycenter of a 3D ROI in meters, the focal length (c) and the coordinates
of the principal point (xo,yo) in mm. Along with the name of a .txt file containing the names of the images and the dimensions of the images in mm (along X and Y direction). Examples of the two .txt files are 
provided in the folder `data`. 

`external_parameters_test1.txt` has the following format:
- 'Xo' (m) : 1st column 
- 'Yo' (m) : 2nd column
- 'Zo' (m) : 3rd column
- 'roll' (degrees) : 4th column
- 'pitch' (degrees) : 5th column
- 'yaw' (degrees) : 6th column

While, `images.txt` contains a column with all the names of the available images.

It is important to notice that the sequence of the images in the two .txt files (`external_parameters_test1.txt` and `images.txt`) has to be the same.

The selection of the most appropriate images is based on two heuristic rules. The first concerns the distance between the projected center of gravity of the 3D ROI and the center of the image. 
The most appropriate views are those for which the aforementioned distance is minimized, in order to reduce the effects of radial distortion. Furthermore, views have to be as vertical as possible, to diminish occlusions. 

The scripti entitled `example_bestPicture.m` demonstrates the usage of the function. 
