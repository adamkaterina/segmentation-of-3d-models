%the name of the .txt file containing the exterior orientation of each view(in m and
%degrees respectively)
exteriorFile = 'external_Paramaters_test1.txt';

%X,Y,Z coordinates of the barycenter (in m)
X = -11.1129; Y = -97.2228; Z = 106.772; 

%interior orineterion of the camera used (in mm)
x0 = 0.18065465539932246131;
y0 = -0.19361613371705299613;
c = 24.40431094157533209454;

%the name of the .txt file containing the list of available images
imageFile= 'images.txt';

%X and Y dimentions of the frame (in mm)
dimentionX = 22.8096;
dimentionY = 15.2064;

%call the function
[picture1, picture2, picture3] = selectBestPicture(exteriorFile, X, Y, Z, x0, y0, c, imageFile, dimentionX, dimentionY);