%This function returns the three most appropriate views for image segmentation (the outputs
%of the function are three strings). The arguments are the name of a file containing 
%the exterior orientation of each view (X, Y, Z in meters, roll, pitch, yaw in degrees). 
%Along with the X, Y, Z coordinates of the barycenter of a 3D ROI in meters, the focal length (c)
%and the principal point coordinates (xo,yo) in mm. Moreover, the name of a file containing 
%the names of the images and the dimensions of the images in mm (alng X and Y direction). 

function [picture1, picture2, picture3] = selectBestPicture(exteriorFile, X, Y, Z, x0, y0, c, imageFile, imageDimentionX, imageDimentionY)

%load the file containing the exterior orientation for each view
F = dlmread(exteriorFile);

x = zeros(size(F, 1), 1);
y = zeros(size(F, 1), 1);
angles = zeros(size(F, 1), 2);

%compute the projected x,y image coordinates of the ROI on each view and
%the sum of the angles of the exterior orientation for each view
for i = 1 : size(F, 1);
 
    R = zeros(3, 3);
 
    X0 = F(i, 1);
    Y0 = F(i, 2);
    Z0 = F(i, 3);
    omega = F(i, 4) * pi / 180;
    phi = F(i, 5) * pi / 180;
    kappa = F(i, 6) * pi / 180;
 
    R(1, 1) = cos(phi) * cos(kappa);
    R(1, 2) = cos(omega) * sin(kappa) + sin(omega) * sin(phi) * cos(kappa);
    R(1, 3) = sin(omega) * sin(kappa) - cos(omega) * sin(phi) * sin(kappa);
 
    R(2, 1) = - cos(phi) * sin(kappa);
    R(2, 2) = cos(omega) * cos(kappa) - sin(omega) * sin(phi) * sin(kappa);
    R(2, 3) = sin(omega) * cos(kappa) + cos(omega) * sin(phi) * sin(kappa);
 
    R(3, 1) = sin(phi);
    R(3, 2) = - sin(omega) * cos(phi);
    R(3, 3) = cos(omega) * cos(phi);
 
    x(i, 1) = x0 - c * ((X - X0) * R(1, 1) + (Y - Y0) * R(1, 2) + (Z - Z0) * R(1, 3)) / ((X - X0) * R(3, 1) + (Y - Y0) * R(3, 2) + (Z - Z0) * R(3, 3));
    y(i, 1) = y0 - c * ((X - X0) * R(2, 1) + (Y - Y0) * R(2, 2) + (Z - Z0) * R(2, 3)) / ((X - X0) * R(3, 1) + (Y - Y0) * R(3, 2) + (Z - Z0) * R(3, 3));
 
    angles(i, 1) = omega ^ 2 + phi ^ 2 + kappa ^ 2;
    angles(i, 2) = i;
 
end

%compute the distance of the projected ROI from the center of each view
distanceFromCenter = zeros(size(x, 1), 2);

for i = 1 : size(x, 1);
 
    distanceFromCenter(i, 1) = sqrt(x(i, 1) ^ 2 + y(i, 1) ^ 2);
    distanceFromCenter(i, 2) = i;
 
end

%two criteria for most appropriate image selection;
%elimininate oblique images angles and select views with the smallest distance of between 
%the projected ROI and the center of the image

sortedDistances = sortrows(distanceFromCenter, 1);
sortedAngles = sortrows(angles, 1);
distanceScore = zeros(size(angles, 1), 1);
angleScore = zeros(size(angles, 1), 1);

for i = 1 : size(angles, 1)
 
    indexAngle = find(sortedAngles(:, 2) == i);
    angleScore(i, 1) = indexAngle;
 
    indexDistance = find(sortedDistances(:, 2) == i);
    distanceScore(i, 1) = indexDistance;
 
end


finalScore(:, 1) = (angleScore + distanceScore) / 2;
finalScore(:, 2) = distanceFromCenter(:, 1);
finalScore(:, 3) = distanceFromCenter(:, 2);

%select the most appropriate views according to these heuristic rules
finalSortedMatrix = sortrows(finalScore, 1);

l = 1;


%ensure tha that the projected ROI is depicted in the selected image
for i = 1 : size(x, 1);
 
    index = finalSortedMatrix(i, 3);
 
    if (- imageDimentionX / 2 <= x(index, 1) && x(index, 1) <= imageDimentionX / 2) && (- imageDimentionY / 2 <= y(index, 1) && y(index, 1) <= imageDimentionY / 2)
     
        finalSelection(l, 1) = index;
     
        l = l + 1;
    end
end


%return the strings containing the names of the selected views
C = readtable(imageFile);

picture1 = C.Images{finalSelection(1, 1)};
picture2 = C.Images{finalSelection(2, 1)};
picture3 = C.Images{finalSelection(3, 1)};

end


